package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Province;

public interface iProvinceRespository extends JpaRepository <Province , Long> {
    Province getProvinceById(int id) ;
}
