package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.District;
import com.example.demo.Model.Province;
import com.example.demo.Model.Ward;
import com.example.demo.Respository.iDistrictRespository;
import com.example.demo.Respository.iProvinceRespository;
import com.example.demo.Respository.iWardRespository;

@RestController
@CrossOrigin
public class AllApiController {
    
    @Autowired
    iProvinceRespository iProvinceRes;

    @Autowired
    iDistrictRespository iDistrictRes;

    @Autowired
    iWardRespository iWardRes;
    

    @GetMapping("district-info")
    public ResponseEntity<Set<District>> getDistrictList(@RequestParam(name = "provinceId") int provinceId) {
        try {
            Province province =  iProvinceRes.getProvinceById(provinceId);
            if(province!=null){
                return new ResponseEntity<>(province.getDistrict(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("province")
    public ResponseEntity<List<Province>> getProvinceList(){
        try {
            List<Province> provinceList = new ArrayList<>();
            iProvinceRes.findAll().forEach(provinceList::add);
            return new ResponseEntity<>(provinceList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("ward-info")
    public ResponseEntity<Set <Ward>> getWardList(@RequestParam(name = "districtId") int districtId) {

        
        //return new ResponseEntity<>(iDistrictRes.findById(wardId) , HttpStatus.OK);
        try {
            District district =  iDistrictRes.getDistrictById(districtId);
            if(district != null) {
                return new ResponseEntity<>(district.getWards(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
     }
}
